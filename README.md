# Opssmart EPCIS API Document

OpsSmart API

POST
OpsSmart EPCIS Simple API

Url : https://www.opssmarttrace.com/OpsDemo-GDST/API/EPCIS/AquaCulture

Url : https://www.opssmarttrace.com/OpsDemo-GDST/API/EPCIS/WildFish


## Method POST
### Request

**Authorization** Bearer Token

| Key | Value |
| ------ | ------ |
| Token | xxx |

`Token` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; encrypted Bearer Token provided by OpsSmart (OpsSmart API Token)

### Body
`json body`
```
{
    "query": {
    "MATCH_anyEPC": [
        "urn:gdst:example.org:product:lot:class:[OrgCode].[ProductCode].[ProductLotNumber]"
        ]
    },
    "queryType": "events"
}
```


## Example
### Request
```
POST /OpsDemo-GDST/API/EPCIS/AquaCulture HTTP/1.1
POST /OpsDemo-GDST/API/EPCIS/WildFish HTTP/1.1
Host: www.opssmarttrace.com
Content-Length: 191

{
    "query": {
        "MATCH_anyEPC": [
            "urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110"
        ]
    },
    "queryType": "events"
}
```

### Response
`Content-Type : text/xml`
```
<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<epcis:EPCISDocuments xmlns:epcis="urn:epcglobal:epcis:xsd:1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sbdh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:cbvmda="urn:epcglobal:cbv:mda" xmlns:gdst="https://traceability-dialogue.org/epcis">
    <EPCISHeader>
        <sbdh:StandardBusinessDocumentHeader>
            <sbdh:HeaderVersion>1.0</sbdh:HeaderVersion>
            <sbdh:Sender>
                <sbdh:Identifier></sbdh:Identifier>
                <sbdh:ContactInformation>
                    <sbdh:Contact></sbdh:Contact>
                    <sbdh:EmailAddress></sbdh:EmailAddress>
                </sbdh:ContactInformation>
            </sbdh:Sender>
            <sbdh:Receiver>
                <sbdh:Identifier></sbdh:Identifier>
                <sbdh:ContactInformation>
                    <sbdh:Contact></sbdh:Contact>
                    <sbdh:EmailAddress></sbdh:EmailAddress>
                </sbdh:ContactInformation>
            </sbdh:Receiver>
            <sbdh:DocumentIdentification>
                <sbdh:Standard>GS1</sbdh:Standard>
                <sbdh:TypeVersion>3.0</sbdh:TypeVersion>
                <sbdh:InstanceIdentifier>9999</sbdh:InstanceIdentifier>
                <sbdh:Type>Seafood Traceability</sbdh:Type>
                <sbdh:MultipleType>false</sbdh:MultipleType>
                <sbdh:CreationDateAndTime></sbdh:CreationDateAndTime>
            </sbdh:DocumentIdentification>
        </sbdh:StandardBusinessDocumentHeader>
        <extension>
            <EPCISMasterData>
                <VocabularyList>
                    <Vocabulary type="urn:epcglobal:epcis:vtype:EPCClass">
                        <VocabularyElementList>
                            <VocabularyElement id="urn:gdst:example.org:product:class:AF-TESCO-01.8853456789090">
                                <attribute id="urn:epcglobal:cbv:mda#descriptionShort">Red Shrimp</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#speciesForFisheryStatisticsPurposesName">Argentine red shrimp</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#additionalTradeItemIdentification">Pleoticus muelleri</attribute>
                            </VocabularyElement>
                            <VocabularyElement id="urn:gdst:example.org:product:class:AF-TESCO-01.1001573458123 2">
                                <attribute id="urn:epcglobal:cbv:mda#descriptionShort">Shrimp (packed, 2kg)</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#speciesForFisheryStatisticsPurposesName"></attribute>
                                <attribute id="urn:epcglobal:cbv:mda#additionalTradeItemIdentification"></attribute>
                            </VocabularyElement>
                        </VocabularyElementList>
                    </Vocabulary>
                    <Vocabulary type="urn:epcglobal:epcis:vtype:Location">
                        <VocabularyElementList>
                            <VocabularyElement id="urn:gdst:example.org:location:loc:AF-TESCO-01.11001">
                                <attribute id="urn:epcglobal:cbv:mda#name">Wholesale Shrimp</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressOne">1234 Sealand Tower</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressTwo"></attribute>
                                <attribute id="urn:epcglobal:cbv:mda#city">Tokyo</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#state">Tokyo</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#countryCode">Janpan</attribute>
                            </VocabularyElement>
                            <VocabularyElement id="urn:gdst:example.org:location:loc:AF-TESCO-01.FL-V19993568">
                                <attribute id="urn:epcglobal:cbv:mda#name">AR.J.DARN</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressOne">09, FIRST FLOOR</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressTwo"></attribute>
                                <attribute id="urn:epcglobal:cbv:mda#city">GANDHIDHAM</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#state">GUJARAT</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#countryCode">India</attribute>
                            </VocabularyElement>
                            <VocabularyElement id="urn:gdst:example.org:location:loc:AF-TESCO-01.Demo-2">
                                <attribute id="urn:epcglobal:cbv:mda#name">OpsSmart Fishing Company</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressOne">115 Rama 9</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#streetAddressTwo"></attribute>
                                <attribute id="urn:epcglobal:cbv:mda#city">Bangkok</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#state">Bangkok</attribute>
                                <attribute id="urn:epcglobal:cbv:mda#countryCode">Thailand</attribute>
                            </VocabularyElement>
                        </VocabularyElementList>
                    </Vocabulary>
                    <Vocabulary type="urn:epcglobal:epcis:vtype:TradingParty">
                        <VocabularyElementList>
                            <VocabularyElement id="urn:gdst:example.org:party:AF-TESCO-01.11001">
                                <attribute id="urn:epcglobal:cbv:mda#name">Wholesale Shrimp</attribute>
                            </VocabularyElement>
                            <VocabularyElement id="urn:gdst:example.org:party:AF-TESCO-01.FL-V19993568">
                                <attribute id="urn:epcglobal:cbv:mda#name">AR.J.DARN</attribute>
                            </VocabularyElement>
                            <VocabularyElement id="urn:gdst:example.org:party:AF-TESCO-01.Demo-2">
                                <attribute id="urn:epcglobal:cbv:mda#name">OpsSmart Fishing Company</attribute>
                            </VocabularyElement>
                        </VocabularyElementList>
                    </Vocabulary>
                </VocabularyList>
            </EPCISMasterData>
        </extension>
    </EPCISHeader>
    <EPCISBody>
        <EventList>
            <ObjectEvent>
                <eventTime>2020-07-26T15:21:00.000Z</eventTime>
                <eventTimeZoneOffset>--7:00</eventTimeZoneOffset>
                <baseExtension>
                    <eventID>CA-0000000130</eventID>
                </baseExtension>
                <action>ADD</action>
                <bizStep>urn:gdst:bizStep:fishingEvent</bizStep>
                <disposition>urn:epcglobal:cbv:disp:active</disposition>
                <bizLocation>
                    <id>urn:gdst:example.org:location:loc:AF-TESCO-01.FL-V19993568</id>
                </bizLocation>
                <extension>
                    <quantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110</epcClass>
                            <quantity>871</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </quantityList>
                    <sourceList>
                        <source type="urn:epcglobal:cbv:sdt:owning_party">urn:gdst:example.org:party:AF-TESCO-01.Demo-2</source>
                    </sourceList>
                    <destinationList>
                        <destination type="urn:epcglobal:cbv:sdt:owning_party">urn:epc:id:sgln:AF-TESCO-01.11001</destination>
                    </destinationList>
                    <ilmd>
                        <cbvmda:vesselCatchInformationList>
                            <cbvmda:vesselCatchInformation>
                                <cbvmda:vesselName>AR.J.DARN</cbvmda:vesselName>
                                <cbvmda:vesselID>24187724</cbvmda:vesselID>
                                <gdst:imoNumber>AR20154841</gdst:imoNumber>
                                <cbvmda:vesselFlagState>India</cbvmda:vesselFlagState>
                                <gdst:vesselPublicRegistry>www.opsmarine.com</gdst:vesselPublicRegistry>
                                <gdst:gpsAvailability>true</gdst:gpsAvailability>
                                <gdst:satelliteTrackingAuthority>OpsSmart</gdst:satelliteTrackingAuthority>
                                <cbvmda:economicZone>Thailand</cbvmda:economicZone>
                                <gdst:fisheryImprovementProject>Example Fishery Improvement Project</gdst:fisheryImprovementProject>
                                <gdst:rmfoArea>India</gdst:rmfoArea>
                                <gdst:subnationalPermitArea>Arabian Sea</gdst:subnationalPermitArea>
                                <cbvmda:catchArea>100</cbvmda:catchArea>
                                <cbvmda:fishingGearTypeCode>1200</cbvmda:fishingGearTypeCode>
                            </cbvmda:vesselCatchInformation>
                        </cbvmda:vesselCatchInformationList>
                        <cbvmda:harvestEndDate>1900-01-01T21:45:00.000Z</cbvmda:harvestEndDate>
                        <cbvmda:harvestStartDate>1900-01-01T21:45:00.000Z</cbvmda:harvestStartDate>
                        <cbvmda:certificationList>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:fishingAuth</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Fishing STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Fishing  Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Fishing-FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Fishing ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:humanPolicy</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Human Welfare STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Human Welfare Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Human Welfare -FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Human Welfare ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCert</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Harvest STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest -FL-V19993568e</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:processorLicense</gdst:certificateType>
                                <cbvmda:certificationStandard>Certification</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Processor Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Processor FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Processor ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCoC</gdst:certificateType>
                                <cbvmda:certificationStandard>Harvest Chain STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Chain Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest Chain FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest Chain ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                        </cbvmda:certificationList>
                    </ilmd>
                </extension>
                <gdst:humanWelfarePolicy>Human Welfare Policy STD</gdst:humanWelfarePolicy>
                <gdst:productOwner>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</gdst:productOwner>
                <cbvmda:informationProvider>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</cbvmda:informationProvider>
            </ObjectEvent>
            <extension>
                <TransformationEvent>
                    <eventTime>2020-11-27T17:00:00.000Z</eventTime>
                    <eventTimeZoneOffset>--7:00</eventTimeZoneOffset>
                    <baseExtension>
                        <eventID>SO0000000006</eventID>
                    </baseExtension>
                    <bizStep>urn:epcglobal:cbv:bizstep:commissioning</bizStep>
                    <disposition>urn:epcglobal:cbv:disp:active</disposition>
                    <bizLocation>
                        <id>urn:gdst:example.org:location:loc:AF-TESCO-01.FL-V19993568</id>
                    </bizLocation>
                    <inputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110</epcClass>
                            <quantity>871</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </inputQuantityList>
                    <outputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-A</epcClass>
                            <quantity>303</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-B</epcClass>
                            <quantity>420</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-C</epcClass>
                            <quantity>148</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </outputQuantityList>
                    <ilmd>
                        <cbvmda:productionDate>2020-11-27T00:00:00.000Z</cbvmda:productionDate>
                        <cbvmda:itemExpirationDate></cbvmda:itemExpirationDate>
                        <cbvmda:countryOfOrigin>Thailand</cbvmda:countryOfOrigin>
                        <cbvmda:certificationList>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:fishingAuth</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Fishing STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Fishing  Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Fishing-FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Fishing ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:humanPolicy</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Human Welfare STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Human Welfare Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Human Welfare -FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Human Welfare ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCert</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Harvest STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest -FL-V19993568e</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:processorLicense</gdst:certificateType>
                                <cbvmda:certificationStandard>Certification</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Processor Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Processor FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Processor ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCoC</gdst:certificateType>
                                <cbvmda:certificationStandard>Harvest Chain STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Chain Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest Chain FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest Chain ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                        </cbvmda:certificationList>
                    </ilmd>
                    <gdst:humanWelfarePolicy>Human Welfare Policy STD</gdst:humanWelfarePolicy>
                    <gdst:productOwner>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</gdst:productOwner>
                    <cbvmda:informationProvider>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</cbvmda:informationProvider>
                </TransformationEvent>
            </extension>
            <extension>
                <TransformationEvent>
                    <eventTime>2020-11-28T18:10:00.000Z</eventTime>
                    <eventTimeZoneOffset>--7:00</eventTimeZoneOffset>
                    <baseExtension>
                        <eventID>DE0000000013</eventID>
                    </baseExtension>
                    <bizStep>urn:epcglobal:cbv:bizstep:commissioning</bizStep>
                    <disposition>urn:epcglobal:cbv:disp:active</disposition>
                    <bizLocation>
                        <id>urn:gdst:example.org:location:loc:AF-TESCO-01.FL-V19993568</id>
                    </bizLocation>
                    <inputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-A</epcClass>
                            <quantity>303</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </inputQuantityList>
                    <outputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-DA</epcClass>
                            <quantity>245</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </outputQuantityList>
                    <ilmd>
                        <cbvmda:productionDate>2020-11-28T00:00:00.000Z</cbvmda:productionDate>
                        <cbvmda:itemExpirationDate></cbvmda:itemExpirationDate>
                        <cbvmda:countryOfOrigin>Thailand</cbvmda:countryOfOrigin>
                        <cbvmda:certificationList>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:fishingAuth</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Fishing STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Fishing  Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Fishing-FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Fishing ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:humanPolicy</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Human Welfare STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Human Welfare Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Human Welfare -FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Human Welfare ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCert</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Harvest STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest -FL-V19993568e</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:processorLicense</gdst:certificateType>
                                <cbvmda:certificationStandard>Certification</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Processor Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Processor FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Processor ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCoC</gdst:certificateType>
                                <cbvmda:certificationStandard>Harvest Chain STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Chain Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest Chain FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest Chain ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                        </cbvmda:certificationList>
                    </ilmd>
                    <gdst:humanWelfarePolicy>Human Welfare Policy STD</gdst:humanWelfarePolicy>
                    <gdst:productOwner>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</gdst:productOwner>
                    <cbvmda:informationProvider>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</cbvmda:informationProvider>
                </TransformationEvent>
            </extension>
            <extension>
                <TransformationEvent>
                    <eventTime>2020-11-29T18:40:00.000Z</eventTime>
                    <eventTimeZoneOffset>--7:00</eventTimeZoneOffset>
                    <baseExtension>
                        <eventID>PA0000000087</eventID>
                    </baseExtension>
                    <bizStep>urn:epcglobal:cbv:bizstep:commissioning</bizStep>
                    <disposition>urn:epcglobal:cbv:disp:active</disposition>
                    <bizLocation>
                        <id>urn:gdst:example.org:location:loc:AF-TESCO-01.FL-V19993568</id>
                    </bizLocation>
                    <inputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.8853456789090.CA-BL-0000000110-DA</epcClass>
                            <quantity>245</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </inputQuantityList>
                    <outputQuantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.1001573458123 2.10015734581232-15</epcClass>
                            <quantity>245</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </outputQuantityList>
                    <ilmd>
                        <cbvmda:productionDate>2020-11-29T00:00:00.000Z</cbvmda:productionDate>
                        <cbvmda:itemExpirationDate>2013-01-01T00:00:00.000Z</cbvmda:itemExpirationDate>
                        <cbvmda:countryOfOrigin></cbvmda:countryOfOrigin>
                        <cbvmda:certificationList>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:fishingAuth</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Fishing STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Fishing  Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Fishing-FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Fishing ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:humanPolicy</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Human Welfare STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Human Welfare Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Human Welfare -FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Human Welfare ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCert</gdst:certificateType>
                                <cbvmda:certificationStandard>AR.J.DARN Harvest STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest -FL-V19993568e</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:processorLicense</gdst:certificateType>
                                <cbvmda:certificationStandard>Certification</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Processor Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Processor FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Processor ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                            <cbvmda:certification>
                                <gdst:certificateType>urn:gdst:cert:harvestCoC</gdst:certificateType>
                                <cbvmda:certificationStandard>Harvest Chain STD</cbvmda:certificationStandard>
                                <cbvmda:certificationAgency>Harvest Chain Agency</cbvmda:certificationAgency>
                                <cbvmda:certificationValue>Harvest Chain FL-V19993568</cbvmda:certificationValue>
                                <cbvmda:certificationIdentification>Harvest Chain ID</cbvmda:certificationIdentification>
                            </cbvmda:certification>
                        </cbvmda:certificationList>
                    </ilmd>
                    <gdst:humanWelfarePolicy>Human Welfare Policy STD</gdst:humanWelfarePolicy>
                    <gdst:productOwner>urn:gdst:example.org:party:AF-TESCO-01.OPS-DEMO2</gdst:productOwner>
                    <cbvmda:informationProvider>urn:gdst:example.org:party:AF-TESCO-01.OPS-DEMO2</cbvmda:informationProvider>
                </TransformationEvent>
            </extension>
            <ObjectEvent>
                <eventTime>2020-11-30T15:21:00.000Z</eventTime>
                <eventTimeZoneOffset>--7:00</eventTimeZoneOffset>
                <baseExtension>
                    <eventID>LA0000000023</eventID>
                </baseExtension>
                <action>OBSERVE</action>
                <bizStep>urn:gdst:bizStep:landing</bizStep>
                <disposition>urn:epcglobal:cbv:disp:active</disposition>
                <bizLocation>
                    <id>urn:epc:id:sgln:AF-TESCO-01.9019</id>
                </bizLocation>
                <cbvmda:landingEndDate>2020-11-30T00:00:00.000Z</cbvmda:landingEndDate>
                <cbvmda:landingStartDate>2020-11-30T00:00:00.000Z</cbvmda:landingStartDate>
                <cbvmda:unloadingPort>Tuna Port</cbvmda:unloadingPort>
                <extension>
                    <quantityList>
                        <quantityElement>
                            <epcClass>urn:gdst:example.org:product:lot:class:AF-TESCO-01.1001573458123 2.10015734581232-15</epcClass>
                            <quantity>457</quantity>
                            <uom>KGM</uom>
                        </quantityElement>
                    </quantityList>
                    <sourceList>
                        <source type="urn:epcglobal:cbv:sdt:owning_party">urn:gdst:example.org:party:AF-TESCO-01.Demo-2</source>
                    </sourceList>
                    <destinationList>
                        <destination type="urn:epcglobal:cbv:sdt:owning_party">urn:epc:id:sgln:AF-TESCO-01.11001</destination>
                    </destinationList>
                </extension>
                <gdst:humanWelfarePolicy>Human Welfare Policy STD</gdst:humanWelfarePolicy>
                <gdst:productOwner>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</gdst:productOwner>
                <cbvmda:informationProvider>urn:gdst:example.org:party:AF-TESCO-01.Demo-2</cbvmda:informationProvider>
                <gdst:certificationList>
                    <cbvmda:certification>
                        <cbvmda:certificationAgency>Human Agency</cbvmda:certificationAgency>
                        <gdst:certificateType>urn:gdst:cert:humanPolicy</gdst:certificateType>
                        <cbvmda:certificationStandard>Tuna Port Port Human STD</cbvmda:certificationStandard>
                        <cbvmda:certificationValue>Certification 9019</cbvmda:certificationValue>
                        <cbvmda:certificationIdentification>Certification 9019</cbvmda:certificationIdentification>
                    </cbvmda:certification>
                    <cbvmda:certification>
                        <cbvmda:certificationAgency>Landing Agency</cbvmda:certificationAgency>
                        <gdst:certificateType>urn:gdst:cert:landingAuth</gdst:certificateType>
                        <cbvmda:certificationStandard>Tuna Port Port Landing STD</cbvmda:certificationStandard>
                        <cbvmda:certificationValue>Certification-9019</cbvmda:certificationValue>
                        <cbvmda:certificationIdentification>Certification-9019</cbvmda:certificationIdentification>
                    </cbvmda:certification>
                </gdst:certificationList>
            </ObjectEvent>
        </EventList>
    </EPCISBody>
</epcis:EPCISDocuments>
```
